from django.urls import path

from blog import views
from blog.views import page_not_found

urlpatterns = [
    path('', views.index, name='index'),

    path('register/', views.register, name='register'),
    path('login/', views.user_login, name='user_login'),
    path('logout/', views.user_logout, name='logout'),

    path('article/<str:category>/', views.article_list, name='article_list'),
    path('article/<str:category>/<int:id>/', views.article_datail, name='article_datail'),
    path('post_comment/<int:post_id>/', views.post_comment, name='post_comment'),

    path('all_articles/', views.all_articles, name='all_articles'),
    path('img/', views.img, name='img'),

    path('search/', views.search, name='search'),
]
handler404 = page_not_found
