from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy

from blog.models import Author, Article, Category, Tag, Tui, Banner, Link, BlogComment, Image
from . import models

# 修改网页title和站点header。
admin.site.site_header = "iblog.cool后台"
admin.site.site_title = 'iblog.cool后台'
admin.site.index_title = 'iblog.cool后台管理'


@admin.register(Author)
class AuthorAdmin(UserAdmin):
    list_display = ['username', 'author_name', 'email']
    fieldsets = (
        (None, {u'fields': ('username', 'password')}),
        (gettext_lazy('用户信息'),
         {'fields': ('author_name','email')}),
        (gettext_lazy('权限信息'),
         {'fields': ('is_active', 'is_staff', 'is_superuser',
                     'groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (None, {u'fields': ('username', 'password1', 'password2')}),
        (gettext_lazy('用户信息'),
         {'fields': ('author_name','email')}),
        (gettext_lazy('权限信息'),
         {'fields': ('is_active', 'is_staff', 'is_superuser',
                     'groups', 'user_permissions')}),
    )


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'category', 'all_tag', 'art_user', 'views', 'created_time', ]

    def art_user(self, obj):
        # return obj.user.last_name + obj.user.first_name
        return obj.user.author_name

    art_user.short_description = '作者'

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        return super().save_model(request, obj, form, change)

    # 修改多对多的显示格式
    def all_tag(self, obj):
        tag_list = []
        for tag in obj.tag.all():
            tag_list.append(tag.name)
        return '、'.join(tag_list)

    all_tag.short_description = '标签'

    filter_horizontal = ('tag',)
    date_hierarchy = 'created_time'  # 详细时间分层筛选

    '''设置可编辑字段'''
    list_editable = ('category',)
    # 不可修改(但是显示)
    # readonly_fields = ("views",)

    list_filter = ('category', 'user')  # 指定列表过滤器，右边将会出现一个快捷的日期过滤选项，
    search_fields = (u"title",)

    # def get_queryset(self, request):
    #     qs = super().get_queryset(request)
    #     if request.user.is_superuser:
    #         return qs
    #     return qs.filter(author=request.user)

    def get_queryset(self, request):
        return super(ArticleAdmin, self).get_queryset(request).all().order_by("-created_time")

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super(ArticleAdmin, self).get_search_results(request, queryset, search_term)
        try:
            search_term_as_int = int(search_term)
            queryset &= (self.model.objects.filter(gift_rule_id=search_term_as_int) |
                         self.model.objects.filter(user_id=search_term_as_int) |
                         self.model.objects.filter(activity_id=search_term))
        except:
            pass
        return queryset, use_distinct


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Tui)
class TuiAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    list_display = ['text_info', 'img', 'link_url', 'is_active']


@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    list_display = ['name', 'link_url', ]


@admin.register(BlogComment)
class BlogCommentAdmin(admin.ModelAdmin):
    list_display = ['article', 'body', 'created_time']


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ['img_title', 'img', 'create_time', 'is_active']
