from .models import Article, BlogComment
from django import forms


class BlogCommentForm(forms.ModelForm):
    class Meta:
        model = BlogComment

        # fields = ['user_name', 'user_email', 'body']
        fields = ['body', ]


class UserRegisterForm(forms.Form):
    # username = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    password = forms.CharField(required=True, min_length=6, max_length=20, error_messages={
        'required': '密码必须填写',
        'min_length': '密码不能低于6位',
        'max_length': '密码不能超过15位'
    })
    c_password = forms.CharField(required=True, min_length=6, max_length=20, error_messages={
        'required': '密码必须填写',
        'min_length': '密码不能低于6位',
        'max_length': '密码不能超过15位'
    })


class UserLoginForm(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(required=True, min_length=6, max_length=20, error_messages={
        'required': '密码必须填写',
        'min_length': '密码不能低于6位',
        'max_length': '密码不能超过15位'
    })


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ('title', 'category', 'tag', 'body', 'user', 'tui')
