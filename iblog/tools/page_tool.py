from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import request


def page_tools(list, page, per_page):
    paginator = Paginator(list, per_page)
    try:
        page_object = paginator.page(page)
    except PageNotAnInteger:
        page_object = paginator.page(1)
    except EmptyPage:
        page_object = paginator.page(paginator.num_pages)
    return page_object, paginator
