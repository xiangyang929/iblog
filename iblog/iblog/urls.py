from django.contrib import admin
from django.urls import path, include, re_path
from django.views.static import serve

from iblog import settings

urlpatterns = [
    path('accounts/', include('allauth.urls')),
    path('admin/', admin.site.urls),
    path('', include(('blog.urls', 'blog'), namespace='blog')),

    path('ueditor/', include('DjangoUeditor.urls')),  # 添加DjangoUeditor的URL

    re_path(r'^media/(?P<path>.*)$', serve, {"document_root": settings.MEDIA_ROOT}),
]
